var gulp = require('gulp');
var sass = require('gulp-sass');
var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync');

gulp.task('default', ['sass', 'browser-sync'], function () {
  gulp.watch("scss/*.scss", ['sass']);
  //gulp.watch("views/**").on('change', browserSync.reload);
  //gulp.watch(["./*.ejs"], browserSync.reload);
  //"static/js/**/*.js",
});

gulp.task('sass', function () {
  return gulp.src('scss/*.scss')
  .pipe(sass({outputStyle: 'compact'}).on('error',sass.logError))
  //.pipe(prefix("last 2 versions", "> 1%", "ie 8", "Android 2", "Firefox ESR"))
  .pipe(gulp.dest('static/css'))
  //.pipe(browserSync.reload({stream:true}));
  .pipe(browserSync.stream());
});

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync({
    proxy: "http://localhost:3000",
    files: ["static/css/**","views/**"],
    port: 1337
  });
});

gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({
    script: 'app.js',
    watch: ['*.js'],
    ignore: ['.git', 'node_modules', 'bower_components', '.sass-cache', 'static'],
    env: { 'NODE_ENV': 'development' }
  })
  .on('start', function () {
    if (!called) cb();
    called = true;
  })
  .on('restart', function () {
    setTimeout(function() {
      browserSync.reload({ stream: false });
    }, 500);
  });
});