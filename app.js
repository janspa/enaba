var express     = require('express');
var path        = require('path');
var ejs         = require('ejs');
var fs          = require('fs');
var bodyParser  = require('body-parser');
var db          = require('./db/monk');
var config      = require('./config');

// Setup

if (!fs.existsSync(path.join(__dirname,'.installed'))) {
  console.error("Enaba hasn't been correctly set up yet; Try running `node setup`");
  return;
}

console.log("Starting app...");

require('./configlocal')(config);

var app = express();

//app.enable('strict routing');
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));

app.error = function(msg, status, arg) {
  var err = (msg instanceof Error) ? msg : new Error(msg);
  err.status = status || 500;
  return err;
};

db(config.db_server);
app.db = db;

var staticOpts = {
  setHeaders: function(res, path, stat) {
    if (res.req.url.match(/^\/thread/i)) {
      res.type('html');
    }
  }
};

// Middleware
app.use(express.static(path.join(__dirname, 'static'), staticOpts));
app.use(express.static(path.join(__dirname, 'favicons')));
app.use(bodyParser.urlencoded({ extended:false }));


// Routes

require('./routes/thread')(app);
require('./routes/catalog')(app);
require('./routes/post')(app);
require('./routes/admin')(app);

/*app.get('/setup', function(req, res) {
  var errmsgs = '';
  // TODO: create the static directories
  app.db.initialSetup(function(err, result) {
    if (err) {
      console.error(err);
      errmsgs += err.toString() + '\n';
    }
    if (result) {
      var ret = 'Setup complete.';
      if (errmsgs)
        ret += '\nErrors:\n'+errmsgs;
      res.send(ret);
    }
  });
});*/

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  if (app.get('env') == 'development') {
    console.error(req.originalUrl, err);
  }
  res.status(err.status || 500);
  res.send(err.message);
});


// Run server
app.listen(config.port, function () {
  console.log("Server ready on port %d.", config.port);
});