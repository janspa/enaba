module.exports = function (app) {

  app.route('/')
    .get(function(req, res, next) {
      require('../renderer').buildCatalog(app, function(err, html) {
        if (err) return next(app.error(err));
        res.send(html);
      });
    });
};