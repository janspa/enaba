module.exports = function (app) {
  var formatter = require('../formatter');
  var ejshelpers = require('../ejshelpers');

  app.route('/admin')
    .get(function(req, res, next) {
      app.db.getAllTags(function(err, tags) {
        res.render('admin', {
          _: ejshelpers,
          title: 'Enaba Admin Panel',
          tags: tags
        });
      });
    })

    .post(function(req, res, next) {
      if (req.body.submit_add_tag) {
        var tag = { abbreviation: req.body.add_tag_abbr, name: req.body.add_tag_name, primary: !req.body.add_tag_secondary };
        if (!tag.abbreviation || !tag.name) {
          return next(app.error('A tag needs a name and an abbreviation!'));
        }
        app.db.addTag(tag, function(err, data) {
          if (err) return next(app.error('Error adding tag: '+err.toString()));
          return res.redirect('/admin');
        });
      }
      else if (req.body.submit_edit_tag) {
        var tag = { name: req.body.edit_tag_name, primary: !req.body.edit_tag_secondary };
        if (!req.body.tag_to_edit) {
          return next(app.error('You need to choose a tag to edit!'));
        }
        if (!tag.name) {
          return next(app.error('A tag needs a name!'));
        }
        app.db.editTag(req.body.tag_to_edit, tag, function(err, data) {
          if (err) return next(app.error('Error editing tag: '+err.toString()));
          return res.redirect('/admin');
        });        
      }
      else if (req.body.submit_remove_tag) {
        if (!req.body.tag_to_remove) {
          return next(app.error('You need to choose a tag to remove!'));
        }
        app.db.removeTag(req.body.tag_to_remove, function(err, data) {
          if (err) return next(app.error('Error removing tag: '+err.toString()));
          return res.redirect('/admin');
        }); 
      }
    });
};