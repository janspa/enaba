module.exports = function (app) {

  app.route('/thread/:id')
    .get(function(req, res, next) {
      require('../renderer').buildThread(req.params.id, app, function(err, html) {
        if (err) return next(app.error(err));
        require('../fileops').writeStaticThread(req.params.id, html, function(err) {
          if (err) return next(app.error(err));
          res.send(html);
        });
      });
    });
};