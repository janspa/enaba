module.exports = function (app) {
  var path = require('path');
  var async = require('async');
  var multer = require('multer');
  var config = require('../config');
  var utils = require('../utils');
  var renderer = require('../renderer');
  var fileops = require('../fileops');

  function getImageDest(file, thumb) {
    if (!thumb) {
      return path.join(config.dir_root,config.dir_static,config.dir_img,file);
    } else {
      return path.join(config.dir_root,config.dir_static,config.dir_thumb,file);
    }
  }

  app.use(multer({
    limits: {
      files: config.max_images,
      fileSize: config.max_filesize
    },
    putSingleFilesInArray: true,

    onParseEnd: function (req, next) {
      //console.log('Form parsing completed at:', new Date());
      next();
    },
    onError: function (error, next) {
      console.error('Form parsing error:',error);
      next(error);
    }
  }));

  app.route('/post')
    .get(function(req, res, next) {
      return next(app.error(config.error.nopost, 405));
    })

    .post(function(req, res, next) {
      async.waterfall([
        // H-here we go
        function(cb) {
          var post = {
            time: Date.now(),
            comment: '',
            tags: null,
            resto: 0,
            op: false,
            name: '',
            subject: '',
            sage: false
          };

          if (req.body['resto'] != 0) {
            post.resto = parseInt(req.body['resto']);
          } else {
            post.resto = 0;
          }
          post.op = (post.resto === 0);

          post.comment = req.body['comm'] || config.defaultcomment;
          if (!post.comment && (config.force_comment ||
              (post.op && config.force_op_comment))) {
            return cb(app.error(config.error.forcedcomment, 405))
          }
          if (!post.comment && !req.files.uploadimg && config.force_comment_or_image) {
            return cb(app.error(config.error.forcedcommorimg, 405));
          }
          // Trim leading/trailing whitespace
          post.comment = post.comment.trim();
          //TODO: Trim from between too?

          if (post.comment.length > config.post_max_len) {
            return cb(app.error(config.error.posttoolong, 405));
          }

          post.name = (!!req.body['name']) ? req.body['name'] : config.anonymous;
          var tripcode = utils.generateTripcode(post.name);
          post.name = tripcode.name;
          post.trip = tripcode.trip;
          if (post.name.length > config.name_max_len) {
            return cb(app.error(config.error.nametoolong, 405));
          }
          post.subject = (post.op && !!req.body['subj']) ? req.body['subj'] : '';
          if (!post.subject && post.op && config.force_op_subject) {
            return cb(app.error(config.error.forcedsubject, 405));
          } else if (post.subject.length > config.subj_max_len) {
            return cb(app.error(config.error.subjtoolong, 405));
          }
          var opts = req.body['opts'] || '';
          post.sage = (!post.op && opts.toLowerCase() === 'sage');

          post.files = req.files.uploadimg || [];


          // If the post is not op, check if the thread being replied to exists
          if (!post.op) {
            app.db.threadExists(post.resto, function(err, data) {
                if (err || data == null)
                  return cb(app.error(config.error.nosuchthread, 404));
                else
                  return cb(null, post);
              });
          }
          // If the poster is op, check that the tags are correct
          else {
            post.tags = req.body['tags'] || [];
            if (typeof post.tags === 'string')
              post.tags = [post.tags];
            if (post.tags.length < config.tags_min) {
              return cb(app.error(config.error.notenoughtags, 405))
            } else if (post.tags.length > config.tags_max) {
              return cb(app.error(config.error.toomanytags, 405))
            } else {
              // TODO: check tag validity
              return cb(null, post);
            }
          }
        },

        // Do image stuff
        function(post, cb) {
          if (!post.files || post.files.length === 0) {
            if (post.op && config.force_op_image)
              return cb(app.error(config.error.forcedimage, 405));
            // Nothing to do here
            return cb(null, post);
          }
          for (var i = 0; i < post.files.length; i++) {
            var file = post.files[i];
            if (config.allowed_ext.indexOf(file.extension) === -1)
              return cb(app.error(config.error.unallowedext+file.extension, 405));
            file.index = i;
            //TODO: truncate name
            //TODO: get image md5 hash
          }

          var easyimg = require('easyimage');
          // Process the images: Start an async job for each file
          async.each(post.files, function(file, fcb) {
            // Name the image and its thumbnail
            file.newname = post.time+'-'+file.index+'.'+file.extension;
            file.thumbname = post.time+'-'+file.index+'.';
            file.thumbname += config.thumb_ext ? config.thumb_ext : file.extension;
            file.newpath = getImageDest(file.newname);
            file.thumbpath = getImageDest(file.thumbname, true);

            // Move the image to its destination
            utils.move(file.path, file.newpath, function(err) {
              if (err) return fcb(err);
              // Image moved successfully, get some information about it
              // Using [0] to pick only the first frame for animated gifs
              easyimg.info(file.newpath+'[0]').then(function(img) {
                file.size = img.size;
                file.width = img.width;
                file.height = img.height;
                // Make a thumbnail
                easyimg.resize({
                  src:    file.newpath+'[0]',
                  dst:    file.thumbpath,
                  width:  post.op ? config.thumb_op_width  : config.thumb_width,
                  height: post.op ? config.thumb_op_height : config.thumb_height
                }).then( function(img) { fcb(); }, fcb);
              }, fcb);
            });
          }, function(err) {
            if (err) return cb(app.error(err));
            // Images processed
            cb(null, post);
          });
        },

        // Add post to db
        function(post, cb) {
          app.db.post(post, function(err, data) {
            if (err) {
              return cb(app.error('Failure inserting post: '+err.toString()));
            } else {
              post.opNumber = (post.op) ? data.number : post.resto;
              return cb(null, post);
            }
          });
        },

        // Bump thread if needed
        function(post, cb) {
          if (!post.op && !post.sage) {
            app.db.bumpThread(post.resto)
              .success(function() { cb(null, post); })
              .error(function() { cb(app.error('Failure bumping thread: '+err.toString())) });
          } else {
            return cb(null, post);
          }
        },

        // (Re)build the thread
        function(post, cb) {
          renderer.buildThread(post.opNumber, app, function(err, html) {
            if (err) return cb(err);
            fileops.writeStaticThread(post.opNumber, html, function(err) {
              if (err) return cb(err);
              return cb(null, post);
            });
          });
        }

      ], function(err, post) {
        if (err) {
          return next(err);
        }
        res.redirect('/thread/'+post.opNumber);
      });

    })
  ;
};