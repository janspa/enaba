var config = require('./config');

var formatter = module.exports = {};

formatter.date = function(n) {
  var dt = new Date(n);
  var d = {
    y: dt.getUTCFullYear(),
    m: dt.getUTCMonth(),
    d: dt.getUTCDate(),
    h: dt.getUTCHours(),
    i: dt.getUTCMinutes(),
    s: dt.getUTCSeconds()
  };
  if (d.m < 10) d.m = '0'+d.m;
  if (d.d < 10) d.d = '0'+d.d;
  if (d.h < 10) d.h = '0'+d.h;
  if (d.i < 10) d.i = '0'+d.i;
  if (d.s < 10) d.s = '0'+d.s;
  return d.y+'/'+d.m+'/'+d.d+' '+d.h+':'+d.i+':'+d.s;
};

formatter.message = function(msg) {
  var sanitizer = require('sanitizer');
  // Sanitize the message
  msg = sanitizer.escape(msg);
  // Parse quotes
  msg = msg.replace(/(^|\n)&gt;(.+)(\n|$)/mg, '$1<span class="quote">&gt;$2</span>$3');
  // Add quote links
  msg = msg.replace(/&gt;&gt;(\d+)/g, '<a class="quotelink" href="#p$1">&gt;&gt;$1</a>');
  // Turn newlines into html linebreaks
  msg = msg.replace(/(\n|\r\n)/g, '<br>$1');
  return msg;
};

formatter.dbTagsToMap = function(tags) {
  var ret = {};
  for (var i = 0; i < tags.length; i++) {
    ret[tags[i]._id] = tags[i].name;
  }
  return ret;
};

formatter.tags = function(post, tagmap) {
  var tAbbr, tPair;
  post.fTags = {};
  for (var i = 0; i < post.tags.length; i++) {
    tAbbr = post.tags[i];
    post.fTags[tAbbr] = tagmap[tAbbr];
  }
};

function formatFileSize(a) {
  if (a < 1024)
    return a+' B';
  else if (a < 1048576)
    return (Math.floor(a*100/1024)/100)+' KB';
  else
    return (Math.floor(a*100/1048576)/100)+' MB';
}

function truncateFileName(a) {
  var match = a.match(/^(.+)(\..+)$/);
  if (!match) // what the hell
    return a;
  var name = match[1];
  var ext = match[2];
  if (name.length < config.max_filename_display)
    return a;
  name = name.substr(0, config.max_filename_display)+'(...)';
  return name+ext;
}

formatter.files = function(post) {
  if (post.images) {
    for (var i = 0; i < post.images.length; i++) {
      post.images[i].fSize = formatFileSize(post.images[i].size);
      post.images[i].fTruncatedname = truncateFileName(post.images[i].originalname);
    }
  }
};

formatter.post = function(post) {
  post.fDate = formatter.date(post.time);
  post.fMessage = formatter.message(post.comment);
  formatter.files(post);
};