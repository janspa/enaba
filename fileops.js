var fs = require('fs');
var path = require('path');
var async = require('async');
var config = require('./config');

var fileops = {};

fileops.initialSetup = function(done) {
  fs.unlink('.installed', function(err) {
    // don't care if .installed actually existed or not
    console.log('Creating static directories...');
    async.each([config.dir_img, config.dir_thumb, config.dir_thread],
      function(item, cb) {
        var dirpath = path.join(__dirname,config.dir_static,item);
        fs.mkdir(dirpath, function(err) {
          if (err) {
            if (err.code == 'EEXIST') {
              console.log(' Directory '+dirpath+' already exists, ignoring');
              return cb();
            }
            return cb(err);
          }
          console.log(' Directory '+dirpath+' created');
          return cb();
        });
      }, function(err) {
        if (err) return done(err);
        console.log('Static directories created.');
        return done();
      });
  });
};

fileops.finishInstallation = function(done) {
  fs.writeFile('.installed', '', done);
};

fileops.writeStaticThread = function(id, html, done) {
  fs.writeFile(path.join(__dirname,config.dir_static,config.dir_thread,id+''), html, function(err) {
    if (err) return done(err);
    return done(null, html);
  });
};

fileops.deleteFile = function(filename, dir, done) {
  fs.unlink(path.join(__dirname,config.dir_static,dir,filename), done);
};
fileops.deleteImage = function(filename, done) {
  fileops.deleteFile(filename, config.dir_img, done);
};
fileops.deleteThumb = function(filename, done) {
  fileops.deleteFile(filename, config.dir_thumb, done);
};
fileops.deleteThread = function(filename, done) {
  fileops.deleteFile(filename, config.dir_thread, done);
};


module.exports = fileops;