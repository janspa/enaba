var utils = module.exports = {};

String.prototype.translate = function(a, b) {
  var c = this;
  for(var i in a)
    c = c.replace(a[i],b[i],'g');
  return c;
};

utils.generateTripcode = function(name) {
  var match = name.match(/^([^#]+)?(##|#)(.+)$/);
  if (!match)
    return {name:name};
  name = match[1] || '';
  var secure = match[2]=='##';
  var trip = match[3];

  // Secure trips not implemented yet, so...
  secure = false;

  // convert to shift_jis
  trip = require('jconv').convert(trip, 'UTF8', 'SJIS').toString();

  // getting salty
  var salt = (trip+'H..').substr(1, 2);
  salt = salt.replace(/[^.-z]/g, '.');
  salt = salt.translate(':;<=>?@[\]^_`', 'ABCDEFGabcdef');

  var javacrypt = require('./lib/javacrypt');
  if (secure) {
    return {name:name, trip:'!!'+javacrypt.crypt(salt, trip)[0].substring(3)};
  } else {
    return {name:name, trip:'!'+javacrypt.crypt(salt, trip)[0].substring(3)};
  }
};

utils.move = function(src, dest, cb) {
  var fs = require('fs');
  fs.rename(src, dest, function(err) {
    if (err) {
      if (err.code === 'EXDEV') {
        // rename failed, fall back to copying instead
        fs.readFile(src, function(err, data) {
          if (err) return cb(err);
          fs.writeFile(dest, data, function(err) {
            if (err) return cb(err);
            fs.unlink(src, function(err) {
              if (err) return cb(err);
              return cb();
            });
          });
        });
      } else {
        return cb(err);
      }
    } else {
      return cb();
    }
  });
}