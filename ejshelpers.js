var _ = module.exports = {};
var config = require('./config');

_.is = function(a, b, c) {
  if (a) return b;
  else if (c) return c;
  else return '';
};

_.link_to = function(a, b, c) {
  return '<a href="'+a+'"'+_.is(c,' class="'+c+'"')+'>'+b+'</a>';
}

_.img_path = function(a) {
  return '/'+config.dir_img+a;
}
_.thumb_path = function(a) {
  return '/'+config.dir_thumb+a;
}