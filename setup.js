var db = require('./db/monk');
var async = require('async');
var config = require('./config');
var fileops = require('./fileops');
require('./configlocal')(config);

db(config.db_server, function (err) {
  var funcs = [
    fileops.initialSetup,
    db.initialSetup,
    fileops.finishInstallation
  ];

  if (process.argv.length > 2 && process.argv[2] == '-reset') {
    funcs.splice(1, 0, db.dropEverything);
  }

  async.waterfall(funcs, function(err, result) {
    if (err) {
      console.error('Setup ended with an error:\n',err);
    } else {
      console.log('Setup completed succesfully.');
    }
    db.close();
  });
});