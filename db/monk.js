var monk = require('monk');
var async = require('async');
var config = require('../config');

var db = null;

function DbMongo(uri, cb) {
  if (cb == null || typeof cb != 'function')
    cb = DbMongo.onConnect;
  db = monk(uri, cb);

  db.on('error', console.error.bind(console, 'MongoDB error:'));
  //db.once('open', function() {});
}

DbMongo.close = function() {
  if (!db) throw new Error("Database isn't connected.");

  db.close();
};

DbMongo.onConnect = function(err) {
  console.log("MongoDB connected.");
};

DbMongo.initialSetup = function(done) {
  if (!db) throw new Error("Database isn't connected.");

  console.log("Setting up database...");
  async.series([
    function(cb) {
      console.log(' Creating collection "posts_op"');
      db.driver.createCollection('posts_op', cb);
    },
    function(cb) {
      console.log(' Creating collection "posts_reply"');
      db.driver.createCollection('posts_reply', cb);
    },

    // Create a collection for auto-incrementation
    function(cb) {
      console.log(' Creating collection "counters"');
      db.driver.createCollection('counters', function(err, collection) {
        if (err) return cb(err);
        collection.insert({ _id: 'post', seq: 0 }, function(err, result) {
          if (err && err.code === 11000) {
            console.log('Post counter already exists.');
            cb(null, result); // the key already exists so I guess it's fine
          } else {
            cb(err, result);
          }
        });
      });
    },

    // Create a collection for tags
    function(cb) {
      console.log(' Creating collection "tags"');
      db.driver.createCollection('tags', function(err, collection) {
        if (err) return cb(err);
        if (config.default_tags.length > 0) {
          console.log(' Adding default tags');
          collection.insert(config.default_tags, cb);
        } else {
          return cb();
        }
      });
    }

  ],
  function (err, result) {
    if (err) {
      done(err);
    } else {
      console.log("Database set up.")
      done();
    }
  });
};

DbMongo.dropEverything = function(done) {
  if (!db) throw new Error("Database isn't connected.");
  console.log("Dropping the whole database.");
  db.driver.dropDatabase();
  done();
  /*if (!db) throw new Error("Database isn't connected.");
  async.each(['counters','posts_op','posts_reply','tags'], function(item, cb) {
    db.get(item).drop(function (err) {
      if (err.errmsg == 'ns not found') {
        // No problem, the collection didn't exist anyway
        done();
      }
    });
  }, done);*/
};

DbMongo.addTag = function(tag, done) {
  if (!db) throw new Error("Database isn't connected.");

  db.driver.collection('tags').insert(
    {_id:tag.abbreviation, name:tag.name, primary:tag.primary},
    {}, done);
};

DbMongo.editTag = function(id, tag, done) {
  if (!db) throw new Error("Database isn't connected.");

  db.driver.collection('tags').findAndModify(
    {_id:id}, [],
    {_id:id, name:tag.name, primary:tag.primary},
    {new:true}, done);
};

DbMongo.removeTag = function(id, done) {
  if (!db) throw new Error("Database isn't connected.");
  db.driver.collection('tags').remove({_id:id}, done);
};

function getNextSequence(name, done) {
  if (!db) throw new Error("Database isn't connected.");

  db.driver.collection('counters').findAndModify(
    {_id:name}, [], {$inc:{seq:1}}, {new:true},
    function (err, data) {
      if (err) return done(err);
      done(err, data.seq);
    }
  );
}

DbMongo.post = function(post, done) {
  if (!db) throw new Error("Database isn't connected.");

  var collection = null;
  var dbPost = {
      time:     post.time || Date.now(),
      name:     post.name,
      trip:     post.trip,
      comment:  post.comment,
      images:   []
  };
  if (post.files) {
    for (var i = 0; i < post.files.length; i++) {
      dbPost.images.push({
        name: post.files[i].newname,
        thumbname: post.files[i].thumbname,
        originalname: post.files[i].originalname,
        size: post.files[i].size,
        width: post.files[i].width,
        height: post.files[i].height
      });
    }
  }
  if (post.op) {
    collection = db.get('posts_op');
    dbPost.bump = post.time;
    dbPost.tags = post.tags;
    dbPost.subject = post.subject;
  } else {
    collection = db.get('posts_reply');
    dbPost.sage = post.sage;
    dbPost.resto = post.resto;
  }
  getNextSequence('post', function(err, seq) {
    if (err) return done(err);
    dbPost.number = seq;
    collection.insert(dbPost, done);
  });
};

DbMongo.removeThread = function(threadNum, done) {
  if (!db) throw new Error("Database isn't connected.");

  threadNum = parseInt(threadNum);
  db.get('posts_op').remove({number:threadNum}, function(err, data) {
    if (!err) return done(err);
    // TODO: delete files
    db.get('posts_reply').remove({resto:threadNum}, done);
  });
};

DbMongo.removeReply = function(postNum, done) {
  if (!db) throw new Error("Database isn't connected.");

  postNum = parseInt(postNum);
  // TODO: delete files
  db.get('posts_reply').remove({number:postNum}, done);
};

DbMongo.bumpThread = function(threadNum, bumpTime) {
  if (!db) throw new Error("Database isn't connected.");

  var posts = db.get('posts_op');
  threadNum = parseInt(threadNum);
  return posts.findAndModify({
    query: { number: threadNum },
    update: { $set: { bump: bumpTime || Date.now() } }
  });
};

DbMongo.threadExists = function(threadNum, done) {
  if (!db) throw new Error("Database isn't connected.");

  threadNum = parseInt(threadNum);
  var posts = db.get('posts_op');
  posts.findOne({ number: threadNum }, done);
};

DbMongo.getPosts = function(threadNum, done) {
  if (!db) throw new Error("Database isn't connected.");

  threadNum = parseInt(threadNum);
  async.waterfall([
    function(cb) {
      db.get('posts_op').findOne({ number: threadNum })
        .success(function(data) {
          if (data == null)
            return done(null);
          var thread = {};
          thread.op = data;
          cb(null, thread);
        })
        .error(cb);
    },

    function(thread, cb) {
      DbMongo.getTagNames(thread.op.tags, function(err, data) {
        if (err) return cb(err);
          thread.tags = data;
          cb(null, thread);
      });
    },

    function(thread, cb) {
      db.get('posts_reply').find({ resto: threadNum }, { sort: { number: 1 } })
        .success(function(data) {
          thread.replies = data;
          cb(null, thread);
        })
        .error(cb);
    }
  ], done);
};

DbMongo.getOriginalPosts = function(filter, page, done) {
  if (!db) throw new Error("Database isn't connected.");

  page = parseInt(page);
  var tpp = config.threads_per_page;
  var opPosts = db.get('posts_op');
  opPosts.find({}, { limit: tpp, skip: page*tpp, sort: { bump:-1} }, done);
};

DbMongo.getTagNames = function(tagAbbrArray, done) {
  if (!db) throw new Error("Database isn't connected.");

  db.get('tags').find({ _id: { $in: tagAbbrArray } }, done);
};

DbMongo.getTagsForThread = function(threadNum, done) {
  if (!db) throw new Error("Database isn't connected.");

  db.get('posts_op').findOne({ number: threadNum},
    function(err, data) {
      if (err) return done(err);
      DbMongo.getTagNames(data.tags, done);
    });
};

DbMongo.getAllTags = function(done) {
  if (!db) throw new Error("Database isn't connected.");

  var tags = db.get('tags');
  tags.find({}, { sort: { primary:-1, _id:1 } }, done);
};

module.exports = DbMongo;