var formatter = require('./formatter');
var ejshelpers = require('./ejshelpers');

var renderer = {};

renderer.buildThread = function(id, app, cb) {
    app.db.getPosts(id, function(err, thread) {
      if (thread) {
        formatter.post(thread.op);
        for (var i = 0; i < thread.replies.length; i++) {
          formatter.post(thread.replies[i]);
        }
        app.render('thread', {
            _: ejshelpers,
            title: 'Enaba - '+(thread.op.subject || thread.op.comment),
            threadnum: thread.op.number,
            data: thread
          }, cb);
      } else {
        return cb(err);
      }
  });
};

renderer.buildCatalog = function(app, cb) {
  app.db.getOriginalPosts(null, 0, function(err, threads) {
    if (err) return next(app.error(err));

    app.db.getAllTags(function(err, tags) {
      if (err) return next(app.error(err));

      if (threads) {
        for (var i = 0; i < threads.length; i++) {
          formatter.post(threads[i]);
          formatter.tags(threads[i], formatter.dbTagsToMap(tags));
        }
      }

      app.render('catalog', {
        _: ejshelpers,
        title: 'Enaba',
        data: threads,
        tags: tags
      }, cb);

    });
  });
};
module.exports = renderer;