if (process.argv.length > 2) {
  var start = Date.now();
  var utils = require('./utils');
  var arg = process.argv[2];

  var easyimg = require('easyimage');
  easyimg.info(arg).then(function(img) {
    var w = img.width;
    var h = img.height;
    console.log(calculateAspectRatio(w,h));
    console.log((Date.now()-start)+'ms');
  }, console.error);
}

function calculateAspectRatio(n, d) {
  var gcd = function (a, b) {
    if (b === 0) return a;
    return gcd(b, a % b);
  }, div;
  if (n === d) {
    return '1:1';
  } else if (n > d) {
    div = gcd(n, d);
  } else {
    div = gcd(d, n);
  }
  return (n/div)+':'+(d/div);
}