var config = {};
module.exports = config;

/* Server */

config.port = 3000;

config.db_server =    'localhost/enaba';
config.db_user =      '';
config.db_password =  '';


/* Board */

config.threads_per_page = 10;

/* Posting */

// Default name
config.anonymous = 'Anonymous';
// Default comment
config.defaultcomment = '';
// Unused at the moment
config.secure_trip_salt = "!#¤%&/()123456789qwertyuiop";

// Thread limits
// The amount of replies until the threads stops being bumped
config.reply_limit = 500;
// The amount of images that can be posted in the thread (0 for limitless)
config.image_hard_limit = 250;
// The amount of replies that can be posted in the thread (0 for limitless)
config.reply_hard_limit = 0;

// Forced parts of the posts
config.force_comment = false;
// Either a comment or an image required; has no effect if config.defaultcomment isn't empty
config.force_comment_or_image = true;
config.force_op_comment = false;
config.force_op_image = true;
config.force_op_subject = false;

// Disabled parts of the posts
// Disables the name field and forces anonymous posting
config.disable_field_name    = false;
config.disable_field_options = false;
config.disable_field_subject = false;
config.disable_post_images   = false;
config.disable_post_deletion = false;

// Tags
config.tags_min = 1;
config.tags_max = 4;
config.default_tags = [
  { _id: 'b', name: 'Random', primary: true },
  { _id: 'gen', name: 'general', primary: false }
];

// Limits
config.post_max_len = 2000;
config.name_max_len = 50;
config.subj_max_len = 100;

// Stuff
config.always_noko = true;
config.hide_sage   = false;

// Images

config.thumb_width     = 125;
config.thumb_height    = config.thumb_width;
config.thumb_op_width  = 250;
config.thumb_op_height = config.thumb_op_width;

config.thumb_ext = 'png';

config.allowed_ext = ['jpg', 'jpeg', 'png', 'gif'];
config.allowed_ext_op = false;

config.max_filename_length = 255;
config.max_filename_display = 25;

config.max_images   = 1;
config.max_filesize = 5 * 1024 * 1024; // 5MB
config.max_width    = 10000;
config.max_height   = config.max_width;

config.image_reject_repost = true;

config.webm_allow_sound  = false;
config.webm_max_length   = 120;
config.webm_max_filesize = config.max_filesize;

// Show the image's aspect ratio
config.show_ratio    = false;
// Show the image's original filename
config.show_filename = true;


/* Directories */

config.dir_root   = __dirname;
config.dir_static = 'static/';
config.dir_img    = 'i/';
config.dir_thumb  = 't/';
config.dir_thread = 'thread/';


/* Error messages */

config.error = {};
config.error.nopost           = "You didn't make a post.";
config.error.nosuchthread     = "The thread you're replying to doesn't exist.";
config.error.notenoughtags    = "Not enough tags to post a new thread. Minimum required: "+config.tags_min;
config.error.toomanytags      = "Too many tags to post a new thread. Maximum allowed: "+config.tags_max;
config.error.posttoolong      = "Your post was too long.";
config.error.nametoolong      = "Your name was too long.";
config.error.subjtoolong      = "Your subject was too long.";
config.error.unallowedext     = "You uploaded a file with an unallowed extension: ";
config.error.forcedcomment    = "You must attach a comment to your post.";
config.error.forcedcommorimg  = "You must attach either a comment or an image to your post.";
config.error.forcedimage      = "You must attach an image to your post.";
config.error.forcedsubject    = "You must attach a subject to your post.";